let buttons
const grid = _qs('.grid')
const scoreResultsPlayer = _qs('.score-results-player')
const scoreResultsComputer = _qs('.score-results-computer')
const playAgainButton = _qs('.play-again')
const gameStatusMsg = _qs('.game-status-msg')
const headerTop = _qs('.header-top')
const gameHeaderContainer = _qs('.game-header-container')



// ============== HELPERS AND LOADERS =================
function _qsa(x) {
    return document.querySelectorAll(x)
}
function _qs(x) {
    return document.querySelector(x)   
}
function _id(x){
    return document.getElementById(x)
}
// CREATE NESTED ARRAY TO CHECK PLAYS
function createMatrix (row, col) {
    let counter = 0
    let final = []
    for (var i = 0; i < row; i++) {
        final.push([])
        for (var j = 0; j < col; j++) {
             final[i][j] = counter++
        }
    } 
    return final
 }
// CREATE BUTTONS FOR GAME PLAY
 const loadGameBoard = (row, col) => {
    let counter = 0
        for (var i = 0; i < row; i++) {
            for (var j = 0; j < col; j++) {
                let button = document.createElement('button')
                button.classList.add('btn')
                button.dataset.id = counter
                button.dataset.row = i
                button.dataset.col = j
                counter++
                grid.appendChild(button)
            }
        } 
 }
// ============== HELPERS AND LOADERS END ======================
// =============================================================
// =============================================================


// ============== GAME STATE FUNCTIONS ======================
let gameState = {
    playerTurn: false,
    numberOfPlays: 0,
    playsArray: createMatrix(3,3),
    winningPosition: {
        type: [],
        location: ''
    },
    playerWins: 0,
    computerWins: 0
}

const resetGameState = () => {
    gameState.playerTurn = false
    gameState.numberOfPlays = 0
    gameState.playsArray = createMatrix(3,3)
    gameState.winningPosition = {
        type: [],
        location: ''
    }
}

const updateGameWinState = (winType, location) => {
    gameState.winningPosition.type = winType
    gameState.winningPosition.location = location   
}

// ============== GAME STATE FUNCTIONS END =====================
// =============================================================
// =============================================================


// ============== ROW/COL/DIAG CHECK ============================

const checkRowsAndCols = (arr) => {
    let win = false 
    // ROW CHECK =====
    for (var row = 0; row < arr.length; row++) {
        if(arr[row][0] === arr[row][1] && arr[row][0] === arr[row][2]) {
            updateGameWinState(['row', arr[row][0]], row)
            return true 
        }     
    } 
    // COL CHECK ====
    for (var col = 0; col < arr.length; col++) {
        if(arr[0][col] === arr[1][col] && arr[0][col] === arr[2][col]) {
            updateGameWinState(['col', arr[0][col]], col)
            return true 
        }
    } 
    return win     
}

const checkDiagonal = (arr) => {
    let win = false
    const topLeft = arr[0][0]
    const topRight = arr[0][2]
    const middle = arr[1][1]
    const bottomLeft = arr[2][0]
    const bottomRight = arr[2][2]

    if( topLeft === middle && topLeft === bottomRight) {
        updateGameWinState(['diag', topLeft], 1)
        return true  
    }

    if(topRight === middle && topRight === bottomLeft) {
        updateGameWinState(['diag', topRight], 1)
        return true  
    }
    return false
}

// ============== ROW/COL/DIAG CHECK END========================
// =============================================================
// =============================================================



// ============== ADD TO STATE AND UI ==========================
const placeMoveInStateAndUI = (button, playerType) => {
    let finished = null
    gameState.numberOfPlays++

    if(playerType === 'player') {
        button.textContent = 'X'
        button.style.backgroundColor = "rgb(248, 170, 26)"
    } else {
        button.textContent = 'O'
        button.style.backgroundColor = "rgb(103, 134, 238)"
    }

    button.disabled = true
    let row = parseInt(button.dataset.row)
    let col = parseInt(button.dataset.col)
    gameState.playsArray[row][col] = button.textContent 
    if(gameState.numberOfPlays === 9) {
        finished = false
    } else {
        finished = true
    }
    return finished
}

const getComputerOffense = ()  => {
    //=========================================================================================
    // ****************NEEDS A LOT OF WORK FOR COMPUTER DEFENSE **************************
    //=========================================================================================
    
    //==========================================================================================
    //  ******************ROWS**********************************************************
    //==========================================================================================
       // ROW 0  COL 0, 1, 2 ===================================================================
        // LEFT
        let computerChoice
        let index = 0
        if((buttons[index + 1].innerText === 'O' && buttons[index + 2].innerText === 'O') 
        && (buttons[index].innerText === '')) {
            gameState.playsArray[index][index] = 'O'
            computerChoice = buttons[index]
            return computerChoice
        } 
        // CENTER
        if((buttons[index].innerText === 'O' && buttons[index + 2].innerText === 'O') 
        && (buttons[index + 1].innerText === '')) {
            gameState.playsArray[index][index + 1] = 'O'
            computerChoice = buttons[index + 1]
            return computerChoice
        } 
        // RIGHT
        if((buttons[index].innerText === 'O' && buttons[index + 1].innerText === 'O') 
            && (buttons[index + 2].innerText === '')) {
                gameState.playsArray[index][index + 2] = 'O'
                computerChoice = buttons[index + 2]
                return computerChoice
        }
         
        // ROW 1  COL 0, 1, 2 ===================================================================
        index = 3
        // LEFT
        if((buttons[index + 2].innerText === 'O' && buttons[index + 1].innerText === 'O') 
            && (buttons[index].innerText === '')) {
                gameState.playsArray[1][0] = 'O'
                computerChoice = buttons[index]
                return computerChoice
        } 
    
        // CENTER
        if((buttons[index].innerText === 'O' && buttons[index + 2].innerText === 'O') 
            && (buttons[index + 1].innerText === '')) {
                gameState.playsArray[1][1] = 'O'
                computerChoice = buttons[index + 1]
                return computerChoice
        }
        
        // RIGHT 
        if((buttons[index].innerText === 'O' && buttons[index + 1].innerText === 'O') 
            && (buttons[index + 2].innerText === '')) {
                gameState.playsArray[1][2] = 'O'
                computerChoice = buttons[index + 2]
                return computerChoice
        }
        
        // ROW 2  COL 0, 1, 2 ===================================================================
        index = 6
        // LEFT
        if((buttons[index + 2].innerText === 'O' && buttons[index + 1].innerText === 'O') 
            && (buttons[index].innerText === '')) {
                gameState.playsArray[2][0] = 'O'
                computerChoice = buttons[index]
                return computerChoice
        } 
    
        // CENTER
        if((buttons[index].innerText === 'O' && buttons[index + 2].innerText === 'O') 
            && (buttons[index + 1].innerText === '')) {
                gameState.playsArray[2][1] = 'O'
                computerChoice = buttons[index + 1]
                return computerChoice
        }
        
        // RIGHT 
        if((buttons[index].innerText === 'O' && buttons[index + 1].innerText === 'O') 
            && (buttons[index + 2].innerText === '')) {
                gameState.playsArray[2][2] = 'O'
                computerChoice = buttons[index + 2]
                return computerChoice
        }
    
        //================================================================================
       //  ******************COLS**********************************************************
      //==================================================================================
         index = 0
          // COL 0  ROW 0, 1, 2 ===================================================================
         // TOP
         if((buttons[index].innerText === 'O' && buttons[index + 3].innerText === 'O') 
         && (buttons[index + 6].innerText === '')) {
             gameState.playsArray[2][index] = 'O'
             computerChoice = buttons[index + 6]
             return computerChoice
         }
         // MIDDLE
         if((buttons[index].innerText === 'O' && buttons[index + 6].innerText === 'O') 
         && (buttons[index + 3].innerText === '')) {
             gameState.playsArray[index + 1][index] = 'O'
             computerChoice = buttons[index + 3]
             return computerChoice
         } 
         // BOTTOM
         if((buttons[index + 3].innerText === 'O' && buttons[index + 6].innerText === 'O') 
         && (buttons[index].innerText === '')) {
             gameState.playsArray[index][index] = 'O'
             computerChoice = buttons[index]
             return computerChoice
         }
         index = 1 
         // COL 1  ROW 0, 1, 2 =================================================================== 
         // TOP
         if((buttons[index].innerText === 'O' && buttons[index + 3].innerText === 'O') 
         && (buttons[index + 6].innerText === '')) {
             gameState.playsArray[index + 1][index] = 'O'
             computerChoice = buttons[index + 6]
             return computerChoice
         }
        // MIDDLE
         if((buttons[index].innerText === 'O' && buttons[index + 6].innerText === 'O') 
         && (buttons[index + 3].innerText === '')) {
             gameState.playsArray[index][index] = 'O'
             computerChoice = buttons[index + 3]
             return computerChoice
         } 
        // BOTTOM
         if((buttons[index + 3].innerText === 'O' && buttons[index + 6].innerText === 'O') 
         && (buttons[index].innerText === '')) {
             gameState.playsArray[index - 1][index] = 'O'
             computerChoice = buttons[index]
             return computerChoice
         }   
         index = 2 
         // COL 2  ROW 0, 1, 2 =================================================================== 
         // TOP
         if((buttons[index].innerText === 'O' && buttons[index + 3].innerText === 'O') 
         && (buttons[index + 6].innerText === '')) {
             gameState.playsArray[index][index] = 'O'
             computerChoice = buttons[index + 6]
             return computerChoice
         }
         // MIDDLE
         if((buttons[index].innerText === 'O' && buttons[index + 6].innerText === 'O') 
         && (buttons[index + 3].innerText === '')) {
             gameState.playsArray[index - 1][index] = 'O'
             computerChoice = buttons[index + 3]
             return computerChoice
         } 
    
        // BOTTOM
        if((buttons[index + 3].innerText === 'O' && buttons[index + 6].innerText === 'O') 
        && (buttons[index].innerText === '')) {
            gameState.playsArray[index - index][index] = 'O'
            computerChoice = buttons[index]
            return computerChoice
        } 
    
        //================================================================================
       //  ******************DIAG**********************************************************
      //==================================================================================
        // LEFT
        if((buttons[0].innerText === 'O' && buttons[4].innerText === 'O') 
        && (buttons[8].innerText === '')) {
            gameState.playsArray[2][2] = 'O'
            computerChoice = buttons[8]
            return computerChoice
        } 
    
        if((buttons[0].innerText === 'O' && buttons[8].innerText === 'O') 
        && (buttons[4].innerText === '')) {
            gameState.playsArray[1][1] = 'O'
            computerChoice = buttons[4]
            return computerChoice
        }
        
        if((buttons[4].innerText === 'O' && buttons[8].innerText === 'O') 
        && (buttons[0].innerText === '')) {
            gameState.playsArray[0][0] = 'O'
            computerChoice = buttons[0]
            return computerChoice
        }
    
        // RIGHT
        if((buttons[2].innerText === 'O' && buttons[4].innerText === 'O') 
        && (buttons[6].innerText === '')) {
            gameState.playsArray[2][0] = 'O'
            computerChoice = buttons[6]
            return computerChoice
        } 
    
        if((buttons[2].innerText === 'O' && buttons[6].innerText === 'O') 
        && (buttons[4].innerText === '')) {
            gameState.playsArray[1][1] = 'O'
            computerChoice = buttons[4]
            return computerChoice
        }
    
        if((buttons[4].innerText === 'O' && buttons[6].innerText === 'O') 
        && (buttons[2].innerText === '')) {
            gameState.playsArray[0][2] = 'O'
            computerChoice = buttons[2]
            return computerChoice
        }   
    }


const getComputerDefense = ()  => {
//=========================================================================================
// ****************NEEDS A LOT OF WORK FOR COMPUTER DEFENSE **************************
//=========================================================================================

//==========================================================================================
//  ******************ROWS**********************************************************
//==========================================================================================
   // ROW 0  COL 0, 1, 2 ===================================================================
    // LEFT
    let computerChoice
    let index = 0
    if((buttons[index + 1].innerText === 'X' && buttons[index + 2].innerText === 'X') 
    && (buttons[index].innerText === '')) {
        gameState.playsArray[index][index] = 'O'
        computerChoice = buttons[index]
        return computerChoice
    } 
    // CENTER
    if((buttons[index].innerText === 'X' && buttons[index + 2].innerText === 'X') 
    && (buttons[index + 1].innerText === '')) {
        gameState.playsArray[index][index + 1] = 'O'
        computerChoice = buttons[index + 1]
        return computerChoice
    } 
    // RIGHT
    if((buttons[index].innerText === 'X' && buttons[index + 1].innerText === 'X') 
        && (buttons[index + 2].innerText === '')) {
            gameState.playsArray[index][index + 2] = 'O'
            computerChoice = buttons[index + 2]
            return computerChoice
    }
     
    // ROW 1  COL 0, 1, 2 ===================================================================
    index = 3
    // LEFT
    if((buttons[index + 2].innerText === 'X' && buttons[index + 1].innerText === 'X') 
        && (buttons[index].innerText === '')) {
            gameState.playsArray[1][0] = 'O'
            computerChoice = buttons[index]
            return computerChoice
    } 

    // CENTER
    if((buttons[index].innerText === 'X' && buttons[index + 2].innerText === 'X') 
        && (buttons[index + 1].innerText === '')) {
            gameState.playsArray[1][1] = 'O'
            computerChoice = buttons[index + 1]
            return computerChoice
    }
    
    // RIGHT 
    if((buttons[index].innerText === 'X' && buttons[index + 1].innerText === 'X') 
        && (buttons[index + 2].innerText === '')) {
            gameState.playsArray[1][2] = 'O'
            computerChoice = buttons[index + 2]
            return computerChoice
    }
    
    // ROW 2  COL 0, 1, 2 ===================================================================
    index = 6
    // LEFT
    if((buttons[index + 2].innerText === 'X' && buttons[index + 1].innerText === 'X') 
        && (buttons[index].innerText === '')) {
            gameState.playsArray[2][0] = 'O'
            computerChoice = buttons[index]
            return computerChoice
    } 

    // CENTER
    if((buttons[index].innerText === 'X' && buttons[index + 2].innerText === 'X') 
        && (buttons[index + 1].innerText === '')) {
            gameState.playsArray[2][1] = 'O'
            computerChoice = buttons[index + 1]
            return computerChoice
    }
    
    // RIGHT 
    if((buttons[index].innerText === 'X' && buttons[index + 1].innerText === 'X') 
        && (buttons[index + 2].innerText === '')) {
            gameState.playsArray[2][2] = 'O'
            computerChoice = buttons[index + 2]
            return computerChoice
    }

    //================================================================================
   //  ******************COLS**********************************************************
  //==================================================================================
     index = 0
      // COL 0  ROW 0, 1, 2 ===================================================================
     // TOP
     if((buttons[index].innerText === 'X' && buttons[index + 3].innerText === 'X') 
     && (buttons[index + 6].innerText === '')) {
         gameState.playsArray[2][index] = 'O'
         computerChoice = buttons[index + 6]
         return computerChoice
     }
     // MIDDLE
     if((buttons[index].innerText === 'X' && buttons[index + 6].innerText === 'X') 
     && (buttons[index + 3].innerText === '')) {
         gameState.playsArray[index + 1][index] = 'O'
         computerChoice = buttons[index + 3]
         return computerChoice
     } 
     // BOTTOM
     if((buttons[index + 3].innerText === 'X' && buttons[index + 6].innerText === 'X') 
     && (buttons[index].innerText === '')) {
         gameState.playsArray[index][index] = 'O'
         computerChoice = buttons[index]
         return computerChoice
     }
     index = 1 
     // COL 1  ROW 0, 1, 2 =================================================================== 
     // TOP
     if((buttons[index].innerText === 'X' && buttons[index + 3].innerText === 'X') 
     && (buttons[index + 6].innerText === '')) {
         gameState.playsArray[index + 1][index] = 'O'
         computerChoice = buttons[index + 6]
         return computerChoice
     }
    // MIDDLE
     if((buttons[index].innerText === 'X' && buttons[index + 6].innerText === 'X') 
     && (buttons[index + 3].innerText === '')) {
         gameState.playsArray[index][index] = 'O'
         computerChoice = buttons[index + 3]
         return computerChoice
     } 
    // BOTTOM
     if((buttons[index + 3].innerText === 'X' && buttons[index + 6].innerText === 'X') 
     && (buttons[index].innerText === '')) {
         gameState.playsArray[index - 1][index] = 'O'
         computerChoice = buttons[index]
         return computerChoice
     }   
     index = 2 
     // COL 2  ROW 0, 1, 2 =================================================================== 
     // TOP
     if((buttons[index].innerText === 'X' && buttons[index + 3].innerText === 'X') 
     && (buttons[index + 6].innerText === '')) {
         gameState.playsArray[index][index] = 'O'
         computerChoice = buttons[index + 6]
         return computerChoice
     }
     // MIDDLE
     if((buttons[index].innerText === 'X' && buttons[index + 6].innerText === 'X') 
     && (buttons[index + 3].innerText === '')) {
         gameState.playsArray[index - 1][index] = 'O'
         computerChoice = buttons[index + 3]
         return computerChoice
     } 

    // BOTTOM
    if((buttons[index + 3].innerText === 'X' && buttons[index + 6].innerText === 'X') 
    && (buttons[index].innerText === '')) {
        gameState.playsArray[index - index][index] = 'O'
        computerChoice = buttons[index]
        return computerChoice
    } 

    //================================================================================
   //  ******************DIAG**********************************************************
  //==================================================================================
    // LEFT
    if((buttons[0].innerText === 'X' && buttons[4].innerText === 'X') 
    && (buttons[8].innerText === '')) {
        gameState.playsArray[2][2] = 'O'
        computerChoice = buttons[8]
        return computerChoice
    } 

    if((buttons[0].innerText === 'X' && buttons[8].innerText === 'X') 
    && (buttons[4].innerText === '')) {
        gameState.playsArray[1][1] = 'O'
        computerChoice = buttons[4]
        return computerChoice
    }
    
    if((buttons[4].innerText === 'X' && buttons[8].innerText === 'X') 
    && (buttons[0].innerText === '')) {
        gameState.playsArray[0][0] = 'O'
        computerChoice = buttons[0]
        return computerChoice
    }

    // RIGHT
    if((buttons[2].innerText === 'X' && buttons[4].innerText === 'X') 
    && (buttons[6].innerText === '')) {
        gameState.playsArray[2][0] = 'O'
        computerChoice = buttons[6]
        return computerChoice
    } 

    if((buttons[2].innerText === 'X' && buttons[6].innerText === 'X') 
    && (buttons[4].innerText === '')) {
        gameState.playsArray[1][1] = 'O'
        computerChoice = buttons[4]
        return computerChoice
    }

    if((buttons[4].innerText === 'X' && buttons[6].innerText === 'X') 
    && (buttons[2].innerText === '')) {
        gameState.playsArray[0][2] = 'O'
        computerChoice = buttons[2]
        return computerChoice
    }   
}

const getComputerChoice = () => {
    let computerChoice
    let index = 0
    let searching = true
    while(searching) {
        let randomRow = Math.floor(Math.random() * gameState.playsArray[0].length)
        let randomCol = Math.floor(Math.random() * gameState.playsArray[0].length)
        console.log(gameState.playsArray[randomRow][randomCol])
        if(gameState.playsArray[randomRow][randomCol] >= 0) {
            gameState.playsArray[randomRow][randomCol] = 'O'
            for (let i = 0; i < buttons.length; i++) {
                let buttonRow = parseInt(buttons[i].dataset.row)
                let buttonCol = parseInt(buttons[i].dataset.col)
                if(buttonRow === randomRow && buttonCol === randomCol) {
                    computerChoice = buttons[i]
                }      
            }
            searching = false
        }
    } 
    return computerChoice
}

const endMessage = (msg) => {
    for (let index = 0; index < buttons.length; index++) {
        buttons[index].disabled = true 
    } 
    gameStatusMsg.textContent = msg + ' wins!!!'
    if(msg === 'User') {
        gameState.playerWins++
        scoreResultsPlayer.textContent = gameState.playerWins
    } else if(msg === 'Computer') {
        gameState.computerWins++
        scoreResultsComputer.textContent = gameState.computerWins
    } else {
        gameStatusMsg.textContent = msg + '!!!'
    }
    setTimeout(() => {
        playAgainButton.classList.add('play-again-show')
        gameStatusMsg.classList.add('game-status-msg-show')
    }, 200)

}

const playAgain = () => {
    resetGameState()
    playAgainButton.classList.remove('play-again-show')
    gameStatusMsg.classList.remove('game-status-msg-show')
    grid.innerHTML = ''
    loadGameBoard(3, 3)
}

const handleClick = (e) => {

    let checkedRowsAndCols
    let checkedDiagonal

    let selectedButton = e.target
    let placedMove =  placeMoveInStateAndUI(selectedButton, 'player')

    if(!placedMove) {
        checkedRowsAndCols = checkRowsAndCols(gameState.playsArray)
        checkedDiagonal = checkDiagonal(gameState.playsArray) 
        if(checkedRowsAndCols || checkedDiagonal) {
            endMessage('User')
        } else {
            endMessage('It\`s A Draw' )
        }

    } else {
        checkedRowsAndCols = checkRowsAndCols(gameState.playsArray)
        checkedDiagonal = checkDiagonal(gameState.playsArray)
        if(checkedRowsAndCols || checkedDiagonal) {
            endMessage('User')   
        } else {
            let computerChoice
            let computerOffense = getComputerOffense()
            let computerDefense = getComputerDefense()
            if(computerOffense !== undefined) {
                computerChoice = computerOffense
            } else if(computerDefense !== undefined) {
                computerChoice = computerDefense
            } else {
                computerChoice =  getComputerChoice() 
            }
            setTimeout(() => {
                let computerMove = placeMoveInStateAndUI(computerChoice, 'computer')
            }, 200)
            checkedRowsAndCols = checkRowsAndCols(gameState.playsArray)
            checkedDiagonal = checkDiagonal(gameState.playsArray)
            if(checkedRowsAndCols || checkedDiagonal) {
                endMessage('Computer')              
            } 
        }
    }
}


scoreResultsComputer.textContent = gameState.computerWins
scoreResultsPlayer.textContent = gameState.playerWins

// EVENT LISTENERS
playAgainButton.addEventListener('click', playAgain)

grid.addEventListener('click', (e)=> {
    buttons = _qsa('.btn')
    handleClick(e)
})

window.addEventListener('DOMContentLoaded', () => {
    loadGameBoard(3, 3)
    headerTop.classList.add('header-top-show')
    gameHeaderContainer.classList.add('game-header-container-show')
})




